package DeveloperTests.Utils

import scala.io.Source

object FileUtils {

  def getBibleAsString(): String = {

    val bible = Source.fromURL(getClass.getResource("/The_Bible_Old_and_New_Testaments_KingJames_Version.txt"))
    val result = bible.getLines.mkString("\n")

    bible.close
    result
  }

}
